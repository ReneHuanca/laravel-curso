<h1>Lista de profesiones</h1>

<table>
    <thead>
        <tr>
            <th>FECHA</th>
            <th>NOMBRE</th>
            <th>MODIFICAR</th>
            <th>ELIMINAR</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($profesiones as $profesion)
        <tr>
            <td>{{ $profesion->created_at->format('d-m-Y') }}</td>
            <td>{{ $profesion->nombre }}</td>
            <td>
                <a href="#">Modificar</a>
            </td>
            <td>
                <form>
                    <input type="submit" value="Eliminar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>