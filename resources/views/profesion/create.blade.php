

@if ($errors->any)
    @foreach ($errors->all() as $error)
        {{ $error }}
    @endforeach
@endif


<hr>
<form action="{{ url('profesion/store') }}" method="POST">
    <!-- token de seguridad para evitar el csrf -->
    @csrf
    <label for="nombre">Nombre de la Profesión</label>
    <input type="text" name="nombre" id="nombre" placeholder="Ingrese nombre" value="{{ old('nombre') }}">
    @if ($errors->has('nombre'))
        {{ $errors->first('nombre') }}
    @endif
    <!-- esto hace lo mismo solo ques es mejor tambien aplicable a bootstrap -->
    {{ $errors->first('nombre') }}

    <br>
    <!-- <input type="text" name="area" placeholder="Ingrese apellido"> -->
    <br>
    <input type="submit" value="CREAR NUEVA PROFESION">
</form>