<?php

use App\Http\Controllers\ProfesionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// rutas opcionales
Route::get('/saludo/{nombre?}/idea', function ($nombre = null){  // optional
    if ($nombre) {
        return "Hola ".$nombre;
    } else {
        return "hola desconocido";
    }
});

// validaciones para las rutas
Route::get('usuarios/{id}', function ($id) {
    return 'el id es '.$id;
})->where('id', '[0-9]+');   // validamos numeros de 0-9 y cualquier cantidad 

Route::get('profesion/', 'App\Http\Controllers\ProfesionController@index')->name('profesion.index');
Route::get('profesion/create', [ProfesionController::class, 'create'])->name('profesion.create');
Route::post('profesion/store', [ProfesionController::class, 'store'])->name('profesion.store');

/**
 * php artisan tinker
 * 
 * Es una consola de php puedo poner Profesion::all(); or Profesion::create([...]);
 * Sirve para realizar pruebas insertar o obtener datos
 */