<?php

namespace App\Http\Controllers;

use App\Models\Profesion;
use Illuminate\Http\Request;
use App\Http\Requests\Profesion\StoreProfessionRequest;

class ProfesionController extends Controller
{
    public function index() 
    {
        $profesiones = Profesion::get();
        // print($profesiones);
        // var_dump($profesiones);
        // dd($profesiones);

        return view('profesion.index', ['profesiones' => $profesiones]);
        // return view('profesion.index', compact('profesiones'));
        // return view('profesion.index', ['profesiones']);
    }

    public function create()
    {
        return view('profesion.create');
    }

    public function store(StoreProfessionRequest $request)
    {
        $profesion = $request->validationData();
        Profesion::create($profesion);

        return redirect()->route('profesion.index');
    }
}
