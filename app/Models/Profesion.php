<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    use HasFactory;

    /**
     * Primary key default(id)
     * 
     * @var string
     */
    protected $primaryKey = 'id_profesion';

    
    /**
     * Table name default(profesions)
     * 
     * @var string
     */
    protected $table = 'profesiones';

    /**
     * The attributes that are mass assignable
     * 
     * @var array
     */
    protected $fillable = [
        'nombre',
    ]; 
}
